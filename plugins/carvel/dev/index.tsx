import React from 'react';
import { createDevApp } from '@backstage/dev-utils';
import { carvelPlugin, CarvelPage } from '../src/plugin';

createDevApp()
  .registerPlugin(carvelPlugin)
  .addPage({
    element: <CarvelPage />,
    title: 'Root Page',
    path: '/carvel'
  })
  .render();
