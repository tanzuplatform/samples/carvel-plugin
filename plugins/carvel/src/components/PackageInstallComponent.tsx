import React, { useEffect, useState } from "react";
import { useKubernetesObjects } from "@backstage/plugin-kubernetes";
import { Content, Page, Progress, ResponseErrorPanel } from "@backstage/core-components";
import { Grid } from "@material-ui/core";

export const PackageInstallComponent = () => {
    const { kubernetesObjects, error } = useKubernetesObjects({
        metadata: {
            name: "fish",
            // App Accelerator installed stuff
            annotations: { 'backstage.io/kubernetes-label-selector': "kapp.k14s.io/app=1653153572112585714" }
            // TAP Package managed stuff
            // annotations: { 'backstage.io/kubernetes-label-selector': "kapp.k14s.io/app=1653153542111913712" }
        },
        apiVersion: "v1",
        kind: "Entity"
    });

    const [loading, setLoading] = useState(true); // State to manage loading state
    let filteredObjects: any[] = [];

    useEffect(() => {
        // This useEffect will run whenever kubernetesObjects changes
        // If kubernetesObjects is not undefined or null, setLoading to false
        if (kubernetesObjects !== undefined && kubernetesObjects !== null) {
            setLoading(false);
        }
    }, [kubernetesObjects]);

    if (kubernetesObjects !== undefined) {
        kubernetesObjects.items[0].resources
            .filter((obj) => obj.resources.length)
            .forEach((obj) => {
                console.log(obj);
                obj.resources.forEach((resource) => {
                    return filteredObjects.push(resource);
                })
            })
    }

    return (
        <Page themeId="tool">
            <Content>
                {loading ? ( // Display progress bar while loading
                    <div className="progress-bar-container">
                        <Progress />
                    </div>
                ) :
                    <Grid container spacing={3} direction="column">{
                        (filteredObjects.length > 0 && filteredObjects.map((obj, i) => {
                            let status = { "status": "unknown" };
                            if (obj.status) {
                                status = obj.status;
                            }
                            return <>
                                <Grid item key={i}><strong>Name:</strong> {obj.metadata.name}, <strong>Status:</strong> {Object.entries(status).map(([k, v]) => `${k}: ${v}`).join(', ')}</Grid>
                            </>
                        }))
                    }

                    </Grid>
                }
                {error !== undefined &&
                    <Grid container spacing={3} direction="column">
                        <Grid item>
                            <ResponseErrorPanel error={(new Error(error))} />;
                        </Grid>
                    </Grid>
                }
            </Content>
        </Page>
    );
}