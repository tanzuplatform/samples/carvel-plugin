import React from 'react';
import { Typography, Grid } from '@material-ui/core';
import {
  InfoCard,
  Header,
  Page,
  Content,
  ContentHeader,
  HeaderLabel,
  SupportButton,
} from '@backstage/core-components';
import { PackageInstallComponent } from '../PackageInstallComponent';

export const ExampleComponent = () => (
  <Page themeId="tool">
    <Header title="Welcome to carvel!">
      <HeaderLabel label="Owner" value="Team X" />
      <HeaderLabel label="Lifecycle" value="Alpha" />
    </Header>
    <Content>
      <ContentHeader title="Carvel Package Installed Items">
        <SupportButton>A way to view resources and what component manages them</SupportButton>
      </ContentHeader>
      <Grid container spacing={3} direction="column">
        <Grid item>
          <PackageInstallComponent />
        </Grid>
      </Grid>
    </Content>
  </Page>
);
