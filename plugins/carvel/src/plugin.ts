import { createPlugin, createRoutableExtension } from '@backstage/core-plugin-api';

import { rootRouteRef } from './routes';

export const carvelPlugin = createPlugin({
  id: 'carvel',
  routes: {
    root: rootRouteRef,
  },
});

export const CarvelPage = carvelPlugin.provide(
  createRoutableExtension({
    name: 'CarvelPage',
    component: () =>
      import('./components/ExampleComponent').then(m => m.ExampleComponent),
    mountPoint: rootRouteRef,
  }),
);
